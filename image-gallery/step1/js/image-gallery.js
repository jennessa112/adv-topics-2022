window.addEventListener('load', function(){

  // Here's the data that we'll use to populate the image gallery
  const images = [
    {path: "../images/Desert.jpg", description:"A hot desert.", artist:"Bob Smith", "date": "3/31/2018", tags:["dessert", "landscape"]},
    {path: "../images/Lighthouse.jpg", description:"A lighthouse on the ocean", artist:"Betty Carter", "date": "2/01/2014", tags:["oceian", "landscape"]},
    {path: "../images/Tulips.jpg", description:"Some beautiful tulips", artist:"Bob Smith", "date": "2/14/2015", tags:["plants", "tulips", "landscape"]}
  ];


  // STEP 1
  // Get handles on the elements we need to work with

  const mainImg = document.querySelector("#mainImg");
  const caption = document.querySelector("#caption");
  const btnPrev = document.querySelector("#btnPrev");
  const btnNext = document.querySelector("#btnNext");

  let currentImg = 0;

  // STEP 2
  // Create a function to display an image object



  // STEP 3
  // add an event handler function to the 'next' button
  

  // STEP 4
  // add an event handler function to the 'prev' button
  


});
