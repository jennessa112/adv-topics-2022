<?php
session_start();
// If you want to use session variables you need to make sure that 
// session_start() gets called in every page (put it in the config file)
// session_start() retreives the session data for the id in the PHPSESSID cookie
// If the PHPSESSID cookie is NOT in the request, 
// then it creates a cookie named PHPSESSID and sets the value to a random id 
// The random id is used to identify the session data on the server.


// We'll use this to hash the password (in Step 2)
//$hashedPassword = password_hash("opensesame", PASSWORD_DEFAULT);
//die($hashedPassword);


if($_SERVER['REQUEST_METHOD'] == "POST"){
	
	$email = $_POST['email'] ?? "";
	$password = $_POST['password'] ?? "";

	if($email == "jane@doe.com" && $password == "opensesame"){
		echo("authenticated");
		session_regenerate_id();
		$_SESSION['userName'] = "Jane";
		$_SESSION['authenticated'] = "yes";
		$_SESSION['role'] = "Admin";
	}else{
		echo("Invalid user name or password");
	}
}

// You would inlude this code in all your pages to detect if the visitor has logged in
if(isset($_SESSION['authenticated'])){
	echo("<h2>Hello {$_SESSION['userName']}</h2>");
	echo("PHPSESSID: {$_COOKIE['PHPSESSID']}");
	var_dump($_SESSION);
}else{
	echo("You are currently not logged in");
}

if(isset($_GET['logout'])){
	echo("logging out...");

	// destroy the session cookie
	if (isset( $_COOKIE[session_name()])){
		setcookie( session_name(), "", time()-3600, "/" );
	}
    //empty the $_SESSION array
	$_SESSION = array();
    //destroy the session on the server
	session_destroy();

	header("location: " . $_SERVER['PHP_SELF']);
}



?>
<!DOCTYPE html>
<html>
<head>
	<title>Simple Authentication</title>
	<script>
	window.addEventListener("load", () => {

		document.getElementById("btnLogout").addEventListener("click", () => {
			let url = "<?php echo($_SERVER['PHP_SELF']);?>"; // NOTE: It's NOT a good idea to mix PHP code into your JS code!!!
			url += "?logout=true";
			alert("About to redirect to:\n" + url);
			location.href = url;
		});

	});

		
	</script>
</head>
<body>
	<h2>Simple Authentication</h2>
	
	<form id="frmLogin" method="POST" action="<?php echo($_SERVER['PHP_SELF']);?>">
		<label>Email</label>
		<br>
		<input type="text" id="txtEmail" name="email" value="jane@doe.com">
		<br>
		<label>Password</label>
		<br>
		<input type="password" id="txtPassword" name="password" value="opensesame">
		<br>
		<input type="submit" value="Log In">
	</form>
	
	<input type="button" value="Log Out" id="btnLogout">

	<p>Checkout a few things after you log in:</p>
	<ul>
		<li>Look at the cookies in the web developer tools (in the Application tab), there is one called <b>PHPSESSID</b></li>
		<li>Look at the Cookie request header, it includes the PHPSESSID</li>
	</ul>
	<p>
		Some things to note:
	</p>
	<ul>
		<li>session_start() sets a Cookie response header that includes the session id (PHPSESSID).</li>
		<li>
			When a Cookie is sent to a browser, it will include the cookie in each subsequent request to the same server.
			So the browser will send the PHPSESSID cookie back in every request to the server.
		</li>
		<li>session_start() checks for a PHPSESSID cookies and retreives the session data that matches the session id.
	</ul>
</body>
</html>