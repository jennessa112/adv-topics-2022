// Vue apps are single page web apps
// Look in index.html
//      It links to the Vue framework
//      It links to main.js (where we'll put all of our code)
//      It has a div with an id of "app", we'll put our Vue app into this div


// Note: after we finish the app, we'll install the Vue Dev Tools for Chrome:
// https://chrome.google.com/webstore/detail/vuejs-devtools/ljjemllljcmogpfapbkkighbhhppjdbg
// We need to 'Allow access to file URLs':
//		1. Click on the extensions icon (it looks like a puzzle piece in the Chrome menu bar)
//		2. Click on the three little buttons next to 'Vue.js dev tools' and choose 'Manage extensions'
//		3. Toggle the switch to 'Allow access to file URLs'


// This is the data that our app will work with
// (normally this would come from a database on the server, but for now I've just hard-coded it here to keep things simple)
var userData = [
    {id:1, firstName:"Jane", lastName:"Doe", email:"jdoe@acme.com"},
    {id:2, firstName:"Tony", lastName:"Thompsom", email:"tony@acme.com"},
    {id:3, firstName:"Jesse", lastName:"Jones", email:"jesse@acme.com"}
];


// To create a simple component in Vue, define an object that 
// has the following properties and methods (there are others, but these are the basic ones)
const rootComponent = {
    
    // The template property defines the UI for the component
    // To display data in the template, use {{ }}
    template:   `<div>
                    <h1>User Manager</h1>
                    <p>Number of users: {{ users.length }}</p>
                    <button @click="addUser">Add a New Users</button>
                    <!-- We'll add a few more Vue components here later-->
                    <user-list 
                        :users="users" 
                        @user-selected="handleUserSelected">
                    </user-list>

                    <user-form
                        v-if="selectedUser"
                        :user="{...selectedUser}"
                        @user-edited="handleUserEdited">
                    </user-form>

                </div>`,
    
    // The data() method should return an object that defines
    // the data that your component will work with (for example, the data that is displayed in the template)
    data(){
        return {
            users: [], // this app will display data about users
            selectedUser: null
        }
    },

    // The mounted() method will get triggered/invoked when the component is added to the DOM
    // (this is a lot like onCreate() in Android)
    mounted(){
        this.users = userData; // normally we'd get data from the server, but I just hard-coded the data in userData
    },

    // Declare any custom methods of the component inside the 'methods' property
    // (for example, event handlers that might be used in the template)
    methods: {
        addUser(){
            this.selectedUser = {};
        },
        handleUserSelected(user){
            //console.log("USER SELECTED: " + user.id)
            this.selectedUser = user;
        },
        handleUserEdited(user){
            //console.log(user);
            if(user.id > 0){
                //UPDATE
                const index = this.users.findIndex((u) => {
                    return u.id == user.id;
                });
                this.users[index] = user;
            }else{
                //INSERT
                user.id = this.users.length;
                this.users.push(user);
            }
        }
    }
};

// Note that there is one other very important property called 'props', which we'll see soon.


// To create a Vue app, call the createApp() method and pass in an object that defines the root component
// (all other components will be added to the root component)
const app = Vue.createApp(rootComponent);

// We'll add a few more components to the app here soon (we need to add them to the app before we mount it below)

////////////////////////////
//User-List component
///////////////////////////

app.component("user-list", {
    props: ["users"],
    template: `
        <div class="user-list">
            <h2>User List</h2>
            <ul>
                <li v-for="user in users" :key="user.id" @click="handleClick(user)">
                    {{ user.firstName + " " + user.lastName }}
                </li>
            </ul>
        </div>
    `,
    methods: {
        handleClick(user){
            //console.log(user)
            this.$emit("user-selected", user)
        }
    }
})

//////////////////////
//User-form component
//////////////////////

app.component("user-form", {
    template: `
        <div class="user-form">
            <h2>User Details</h2>
            <form @submit.prevent="handleSubmit">
                <div>
                    <label>First Name:</label>
                    <input v-model="user.firstName" />
                    <span class="validation" v-if="errors.firstName">{{errors.firstName}}</span>
                </div>
                <div>
                    <label>Last Name:</label>
                    <input v-model="user.lastName" />
                    <span class="validation" v-if="errors.lastName">{{errors.lastName}}</span>
                </div>
                <div>
                    <label>Email:</label>
                    <input v-model="user.email" />
                    <span class="validation" v-if="errors.email">{{errors.email}}</span>
                </div>
                <div>
                    <input type="submit" id="btnSubmit" name="submit button">
                </div>
            </form>
        </div>`,
    props: ["user"],
    data(){
        return{
            errors: {}
        }
    },
    methods: {
        handleSubmit(){
            //console.log("Handle for submit");
            if(this.validate()){
                this.$emit("user-edited", this.user);
            }
        },
        validate() {

            // clear our any error messages from the previous submit
            this.errors = {};

            if (!this.user.firstName) {
                this.errors.firstName = "First name is required";
            }

            if (!this.user.lastName) {
                this.errors.lastName = "Last name is required";
            }
            
            if (!this.user.email) {
                this.errors.email = "Email is required";
            }else if(!this.validateEmailAddress(this.user.email)){
                this.errors.email = "The email address entered is not valid";
            }
            
            // if there are no keys in the errors object, then everything is valid
            return Object.keys(this.errors).length == 0;

        },
        validateEmailAddress(email){
            var regExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regExp.test(email);
        }
    }
})
// and then 'mount' the app to the element on the page that contains the app.
app.mount("#app");